import datetime
import requests
from selenium import webdriver
from ctypes import *
import os
import sys
import pyodbc
import time
import configparser
import uuid as uuid
import random
import http.client
import mimetypes
import urllib
import json
import time
import requests
import subprocess
import time


# get data from config file
def getConfig(section, key):
    config = configparser.ConfigParser()
    inipath = os.path.dirname(os.path.realpath(sys.executable))
    config.read("config.ini", encoding="utf-8")
    # config.read(inipath + "\\config.ini", encoding="utf-8")
    return config.get(section, key)


# get data from vmconfig file 
def getVMConfig(section, key):
    config = configparser.ConfigParser()
    config.read("C:\\abc\\ABCMOBI.ini", encoding="utf-8")
    return config.get(section, key)


Driver = getConfig("database", "Driver")
SERVER = getConfig("database", "SERVER")
DATABASE = getConfig("database", "DATABASE")
UID = getConfig("database", "UID")
PWD = getConfig("database", "PWD")
yzmuser = getConfig("vcode", "username")
yzmpassword = getConfig("vcode", "password")
chromepath = getConfig("chrome", "chromedriverpath")


# Captcha class
class YDMHttp:
    API_URL = 'http://api.yundama.com/api.php'
    username = ''
    password = ''
    appid = ''
    appkey = ''

    def __init__(self, username, password, appid, appkey):
        self.username = username
        self.password = password
        self.appid = str(appid)
        self.appkey = appkey

    def request(self, fields, files=[]):
        response = self.post_url(self.API_URL, fields, files)
        response = json.loads(response)
        return response

    def balance(self):
        data = {'method': 'balance', 'username': self.username, 'password': self.password, 'appid': self.appid,
                'appkey': self.appkey}
        response = self.request(data)
        if (response):
            if (response['ret'] and response['ret'] < 0):
                return response['ret']
            else:
                return response['balance']
        else:
            return -9001

    def login(self):
        data = {'method': 'login', 'username': self.username, 'password': self.password, 'appid': self.appid,
                'appkey': self.appkey}
        response = self.request(data)
        if (response):
            if (response['ret'] and response['ret'] < 0):
                return response['ret']
            else:
                return response['uid']
        else:
            return -9001

    def upload(self, filename, codetype, timeout):
        data = {'method': 'upload', 'username': self.username, 'password': self.password, 'appid': self.appid,
                'appkey': self.appkey, 'codetype': str(codetype), 'timeout': str(timeout)}
        file = {'file': filename}
        response = self.request(data, file)
        if (response):
            if (response['ret'] and response['ret'] < 0):
                return response['ret']
            else:
                return response['cid']
        else:
            return -9001

    def result(self, cid):
        data = {'method': 'result', 'username': self.username, 'password': self.password, 'appid': self.appid,
                'appkey': self.appkey, 'cid': str(cid)}
        response = self.request(data)
        return response and response['text'] or ''

    def decode(self, filename, codetype, timeout):
        cid = self.upload(filename, codetype, timeout)
        if (cid > 0):
            for i in range(0, timeout):
                result = self.result(cid)
                if (result != ''):
                    return cid, result
                else:
                    time.sleep(1)
            return -3003, ''
        else:
            return cid, ''

    def report(self, cid):
        data = {'method': 'report', 'username': self.username, 'password': self.password, 'appid': self.appid,
                'appkey': self.appkey, 'cid': str(cid), 'flag': '0'}
        response = self.request(data)
        if (response):
            return response['ret']
        else:
            return -9001

    def post_url(self, url, fields, files=[]):
        for key in files:
            files[key] = open(files[key], 'rb')
        res = requests.post(url, files=files, data=fields)
        return res.text


# Database class
class DbManager:
    def __init__(self):
        self.driver = Driver
        self.server = SERVER
        self.database = DATABASE
        self.uid = UID
        self.pwd = PWD
        self.conn = None
        self.cur = None

    # Connect to DB
    def connectDatabase(self):
        connection_data = "DRIVER={};SERVER={};DATABASE={};UID={};PWD={}".format('{' + self.driver + '}', self.server,
                                                                                 self.database, self.uid, self.pwd)
        try:
            self.conn = pyodbc.connect(connection_data)
            self.cur = self.conn.cursor()
            return True
        except:
            print("connect fail")
            return False

    # Close DB connection
    def close(self):
        # close if has coonection and data
        if self.conn and self.cur:
            self.cur.close()
            self.conn.close()
        return True

    # execute sql query
    def execute(self, sql, params=None, commit=False, ):
        # Connect to DB
        res = self.connectDatabase()
        if not res:
            return False
        try:
            if self.conn and self.cur:
                # if has connection execute sql query
                rowcount = self.cur.execute(sql)
                self.conn.commit()
                # if commit:
                #     self.conn.commit()
                # else:
                #     pass
        except:
            print("execute failed: " + sql)
            print("params: " + str(params))
            self.close()
            return False
        return rowcount

    # get all data
    def fetchall(self, sql, params=None):
        res = self.execute(sql)
        if not res:
            print("Query not executed")
            return False
        results = self.cur.fetchall()
        # print("жџҐиЇўж€ђеЉџ" + str(results))
        self.close()
        return results

    # get one data
    def fetchone(self, sql, params=None):
        res = self.execute(sql, params)
        if not res:
            print("Query not executed")
            return False
        result = self.cur.fetchone()
        # print("жџҐиЇўж€ђеЉџ" + str(result))
        self.close()
        return result

    # edit data
    def edit(self, sql, params=None):
        res = self.execute(sql, params, True)
        if not res:
            print("Query not executed")
            return False
        self.conn.commit()
        print("Execute success" + str(res))
        self.close()
        return res


# create account
def getaccount(a, b, c, d, e):
    li = [a, b, c, d, e]
    random.shuffle(li)
    # get random value
    return ''.join(li)


# create password
def getpasssord():
    uid = str(uuid.uuid1())
    suid = ''.join(uid.split('-'))
    password4 = random.sample(suid, random.randint(6, 20))
    return ''.join(password4)


def getyzm(filename):
    # login
    username = yzmuser
    # password
    password = yzmpassword
    # app id 
    appid = 1
    # app key
    APP_KEY = '22cc5376925e9387a23cf797cb9ba745'
    # е›ѕз‰‡ж–‡д»¶
    # filename = 'C:\\Users\\lsy\\Desktop\\Pythonи°ѓз”Ёз¤єдѕ‹\\ж–°е»єж–‡д»¶е¤№\\PythonHTTPи°ѓз”Ёз¤єдѕ‹\\getimage.jpg'
    # success code http://www.yundama.com/price.html
    codetype = 1006
    # timeout in seconds
    timeout = 60
    # check for correct username
    if username == 'username':
        print('please set correct username')
    else:
        # init
        yundama = YDMHttp(username, password, appid, APP_KEY)
        # login token
        uid = yundama.login()
        print('uid: %s' % uid)
        # check balance
        balance = yundama.balance()
        print('balance: %s' % balance)
        # get image,code type and timeout
        cid, result = yundama.decode(filename, codetype, timeout)
        print('cid: %s, result: %s' % (cid, result))
        return result


# change resolution
def setscreen(x, y):
    try:
        cdll = os.path.dirname(os.path.realpath(sys.executable)) + "\\Dll1.dll"
        # cdll='C:\\Users\\lsy\\Desktop\\Pythonи°ѓз”Ёз¤єдѕ‹\\Dll1.dll'
        h = windll.LoadLibrary(cdll)
        a = h.setscreen(x, y)
        return
    except:
        return

# download captcha image
def downloadimg(imagurl, imagepath, useragent, cookie):
    try:
        print("downloading image")
        header = {
            'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
            'accept-encoding': 'gzip, deflate, br',
            'accept-language': 'zh-CN,zh;q=0.9,en-US;q=0.8,en;q=0.7',
            'cache-control': 'max-age=0',
            'upgrade-insecure-requests': '1',
            'user-agent': "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36"
        }
        cookiess = [item["name"] + "=" + item["value"] for item in cookie]
        cookiestr = '; '.join(item for item in cookiess)
        header.update({'Cookie': cookiestr})
        header.update({'user-agent': useragent})
        # imgurl = "https://c.mail.ru/c/6?0.556070617381563"
        # imagpath = "D:\\test\\yzm.png"
        r = requests.get(imagurl, headers=header)
        with open(imagepath, 'wb') as f:
            f.write(r.content)
    except Exception as e:
        print(e)


def setintnet(se):
    # 0 set internet connection, 1 reset internet connection
    cmd_connect_command_velcom = ['rasdial',
                                  'Velcom PPPoE', '000203117005', '2063222']
    cmd_disconnect_command_velcom = ['rasdial', 'Velcom PPPoE', '/disconnect']
    if se == 0:
        print("Error. Check your connection")
    elif se == 1:
        subprocess.Popen(cmd_connect_command_velcom, stdout=subprocess.PIPE)
        return True


# resets velcom pppoe
def resetintnet():
    cmd_connect_command_velcom = ['rasdial',
                                  'Velcom PPPoE', '000203117005', '2063222']
    cmd_disconnect_command_velcom = ['rasdial', 'Velcom PPPoE', '/disconnect']
    subprocess.Popen(cmd_disconnect_command_velcom, stdout=subprocess.PIPE)
    time.sleep(2)
    subprocess.Popen(cmd_connect_command_velcom, stdout=subprocess.PIPE)


# execute js script
def execute_script(browser, js):
    try:
        return browser.execute_script(js)
    except:
        return ''


# set random window size
def setbrowszie(browser):
    fbl = [[1024, 600], [1024, 600], [1024, 600], [1024, 768], [1280, 1024], [1600, 900], [1440, 1050], [1600, 1200],
           [1280, 800], [1366, 768], [1280, 854], [1440, 900], [1600, 1024], [1680, 1050], [1920, 1080]]
    random.shuffle(fbl)
    browser.set_window_size(fbl[0][0], fbl[0][1])


# configure driver
def setbrowser(useragent, byid):
    # options init
    options = webdriver.ChromeOptions()
    # set browser language
    options.add_argument('lang=ru-ru.UTF-8')
    options.add_experimental_option('excludeSwitches', ['enable-automation'])
    # set useragent
    options.add_argument('user-agent={}'.format(useragent))
    # set dir to save cookie files
    options.add_argument(r"user-data-dir=" + "C:\\chromedata\\" + byid)
    try:
        browser = webdriver.Chrome(
            chrome_options=options, executable_path=chromepath)
    except Exception as e1:
        print(chromepath + "File doesn't exist")
        print(e1)
        return False
    setbrowszie(browser)
    return browser


# resiter mail.ru
def registermail(vmid, browser, byid, surname, name, mailetype, useragent, yzmpath, dbManagers, starttime):
    year = ""
    month = ""
    day = ""
    account = ""
    password = ""
    var = 1
    logid = str(int(time.time()))
    browser.get("https://account.mail.ru/signup?from=main&rf=auth.mail.ru")
    time.sleep(5)
    # з‚№е‡»жіЁе†ЊжЊ‰й’®
    # browser.find_element_by_id("PH_regLink").click()
    while var < 100:
        try:
            var = var + 1
            time.sleep(1)
            if var > 50:
                print("loop")
                return False
            if len(execute_script(browser, "return document.querySelector('.b-panel__content__desc').innerText;")) > 0:
                print("Need confirmation")
                break
            if len(execute_script(browser, "return document.querySelector('.js-signup-simple-link').innerText;")) <= 0:
                if len(execute_script(browser, "return document.querySelector('.b-phone__number').className;")) > 0:
                    print("Need to provide mobile phone")
                    return False
            # browser.get("https://account.mail.ru/signup?from=navi&lang=ruRU&siteid=169&rnd=585367170")
            # get cookie files
            cookies = browser.get_cookies()
            time.sleep(5)
            # name and surname input
            browser.execute_script(
                "document.querySelector('[name=firstname]').value='{}';".format(surname))
            browser.execute_script(
                "document.querySelector('[name=lastname]').value='{}';".format(name))
            # get random month
            monthrand = str(random.randint(0, 11))
            month = str(random.randint(0, 11) + 1)
            # input month
            browser.execute_script(
                "document.querySelector('.b-date__month').querySelectorAll('.b-dropdown__list__item')[{}].click();".format(monthrand))
            # input random year
            browser.execute_script(
                "document.querySelector('.b-date__year').querySelectorAll('.b-dropdown__list__item')[{}].click();".format(str(random.randint(19, 55))))
            year = browser.execute_script(
                "return document.querySelector('.b-date__year').innerText;")
            # input random day
            browser.execute_script(
                "document.querySelector('.day{}').click();".format(str(random.randint(1, 31))))
            day = browser.execute_script(
                "return document.querySelector('.b-date__day').innerText;")
            # choose random gender
            browser.execute_script("document.querySelectorAll('[name=sex]')[{}].click();".format(
                str(random.randint(0, 1))))
            # select domain
            browser.execute_script(
                "document.querySelector('.b-email__domain').querySelectorAll('.b-dropdown__list__item')[{}].click();".format(str(mailetype)))
            time.sleep(4)
            # login to mail
            browser.execute_script(
                "document.querySelector('.b-email__name').querySelector('.b-input_disallow-custom-domain').value='';")
            acnum = browser.execute_script(
                "return document.querySelectorAll('.b-list__item__content').length;")
            randnum = random.randint(0, int(acnum) - 1)
            account = browser.execute_script(
                "return document.querySelectorAll('.b-list__item__content')[{}].innerText;".format(str(randnum)))
            print(account)
            browser.execute_script(
                "document.querySelectorAll('.b-list__item__content')[{}].click();".format(str(randnum)))
            time.sleep(5)
            # provide password
            password = getpasssord()
            browser.execute_script(
                "document.querySelector('.b-input_plate').value='';")
            browser.find_element_by_class_name(
                "b-input_plate").send_keys(str(password))
            time.sleep(5)
            # repeat password
            browser.execute_script(
                "document.querySelector('[name=password_retry]').value='';")
            browser.find_element_by_name(
                "password_retry").send_keys(str(password))
            time.sleep(5)
            # click to singup button
            execute_script(
                browser, "document.querySelector('.js-signup-simple-link').click();")
            browser.find_element_by_class_name("btn_responsive-wide").click()
            time.sleep(3)
        except:
            continue
    var2 = 1
    yzmnum = 0
    while var2 < 100:
        try:
            var2 = var2 + 1
            time.sleep(1)
            if var2 > 50:
                print("loop")
                return False
            if len(execute_script(browser,
                                  "return document.querySelector('.search-panel-button').innerText;")) > 0 or len(
                    execute_script(browser, "return document.querySelector('.js-next-step-button').innerText;")) > 0:
                print("Register success")
                break

            if len(execute_script(browser, "return document.querySelector('.b-form-field__label').innerText;")) > 0:
                print("Need to provide mobile phone") 
                resetintnet()
                time.sleep(5)
                return False
                # жµ‹иЇ•з”Ёз›ґжЋҐйЂЂе‡є
                # break
            src = execute_script(
                browser, "return document.querySelector('.b-captcha__captcha').src;")
            time.sleep(5)
            # download captcha image
            if len(src) > 0:
                imagepath = yzmpath + "\\" + "yzm.png"
                print(imagepath)
                if os.path.exists(imagepath):
                    print("deleteyzm")
                    os.remove(imagepath)
                print(src)
                downloadimg(src, imagepath, useragent, cookies)
                time.sleep(8)
                if os.path.exists(imagepath):
                    print("startyzm")
                    # get solve code
                    yzmresult = getyzm(imagepath)
                    yzmnum = yzmnum + 1
                    if yzmnum > 10:
                        return False
                    time.sleep(5)
                    print(yzmresult)
                    # input captcha
                    execute_script(browser,
                                   "document.querySelector('.b-input_captcha').value='" + str(yzmresult) + "';")
                    # click to confirm button
                    execute_script(
                        browser, "document.querySelector('.btn_main').click();")
                    time.sleep(10)
                    if len(execute_script(browser,
                                          "return document.querySelector('.b-captcha__error-msg').innerText;")) > 0:
                        print("Login error")
                        browser.exec_js(
                            "document.querySelector('.b-captcha__code__reload').click();")
                        time.sleep(5)
        except:
            continue
    # dbManagers.execute(
    #     "UPDATE Buyer SET birthdayyear='" + year + "',birthdaymonth='" + month + "',EmailRegisteredTime='" + str(
    #         datetime.datetime.now()) + "',birthdayday='" + day + "', Email='" + account + "', PassWord='" + password + "' WHERE BuyerID='" + byid + "'")
    # dbManagers.close()
    if account != "":
        return year, month, day, account, password
    else:
        return False
